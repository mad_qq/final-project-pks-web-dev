<?php

use Illuminate\Http\Request;
use App\Book;
use App\Http\Resources\Book as BookResource;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    Route::post('logout', 'AuthController@logout');
});

Route::prefix('v1')->group(function () { 
    Route::get('books', 'BookController@index');
    Route::get('book/{uuid}', 'BookController@view')->where('id', '[0-9]+');
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::middleware('auth:api')->get('/user', function (Request $request) {
        Route::post('logout', 'AuthController@logout');
    });
});

Route::resource('categories', 'CategoryController');
