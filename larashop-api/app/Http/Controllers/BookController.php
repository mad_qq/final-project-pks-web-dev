<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Book;
use App\Http\Resources\Book as BookResource;
use App\Http\Resources\Books as BookCollectionResource;

class BookController extends Controller
{
    public function cetak($judul)
    {
        return $judul;
    }

    public function index()
    {
        $books = new BookCollectionResource(Book::paginate());
        return $books;
    }

    public function view($uuid)
    {
        //$book = DB::select('select * from books where id = ?', [ $uuid ]);
        $book = new BookResource(Book::find($uuid));
        return $book;
    }
}
