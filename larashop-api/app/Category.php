<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Category extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at']; 
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot() {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });

    }

    protected $fillable = [
        'name', 'slug', 'image', 'status'
    ];
    
    /*public function books()
    {
        return $this->belongsToMany('App\Book', 'book_category', 'category_id', 'book_id');
    }*/

    public function books(){
        return $this->belongsToMany("App\Book");
    }
}
